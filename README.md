## OpenAI AI Text Classifier

This Drupal module provides integration of "OpenAI AI Text Classifier",
as well as a view field to request "OpenAI AI Text Classifier" API.
It's available as a toolbar button and can be added to CKEditor toolbar 
from "Text formats and editors"

[OpenAI AI Text Classifier Tool](https://platform.openai.com/ai-text-classifier)

### Installing module.
1. Use composer to install the module.
2. Enable the module in your preferred way.
3. Navigate to Configuration >> Content authoring >> Text formats and editors.
4. Edit one of your text editor formats.
5. Drag and drop the Text classifier button to your toolbar.
