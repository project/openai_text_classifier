<?php

declare(strict_types=1);

namespace Drupal\openai_text_classifier\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Drupal\openai_text_classifier\TextClassifierHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a 'TextClassifierController' controller class.
 */
class TextClassifierController extends ControllerBase {

  /**
   * TextClassifierHelper Service.
   *
   * @var \Drupal\openai_text_classifier\TextClassifierHelper
   */
  protected $helper;

  /**
   * Cache tag invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected CacheTagsInvalidatorInterface $invalidator;

  /**
   * TextClassifierController constructor.
   *
   * @param \Drupal\openai_text_classifier\TextClassifierHelper $helper
   *   TextClassifierHelper Service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheTagsInvalidator
   *   Cache tags invalidator service.
   */
  public function __construct(TextClassifierHelper $helper, CacheTagsInvalidatorInterface $cacheTagsInvalidator) {
    $this->helper = $helper;
    $this->invalidator = $cacheTagsInvalidator;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openai_text_classifier.helper'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * Handles ckeditor plugin request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request from ckeditor plugin.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Score label in json response.
   */
  public function handle(Request $request): JsonResponse {
    $data = json_decode($request->getContent());
    $text = isset($data->prompt) ? strip_tags($data->prompt) : '';

    return new JsonResponse(["label" => $this->helper->getScoreLabel($text)]);
  }

  /**
   * Handle ajax response.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns ajax replace command with response.
   */
  public function handleAjax(NodeInterface $node): AjaxResponse {
    $label_html = '';
    $selector = "#label-{$node->bundle()}-{$node->id()}";
    $response = new AjaxResponse();

    if ($node->hasField('body')) {
      $text = strip_tags($node->get('body')->value);
      $label = $this->helper->getScoreLabel($text);
      $label_class = Html::cleanCssIdentifier($label);
      $label_html = "<span class='score-circle {$label_class}'></span>{$label}";

      // Save classifier label.
      $this->helper->saveNodeLabel($node, $label);
      // Invalidate cache.
      $this->invalidator->invalidateTags(["classifier_label:{$node->id()}"]);

    }

    $response->addCommand(new HtmlCommand($selector, $label_html));
    return $response;
  }

}
