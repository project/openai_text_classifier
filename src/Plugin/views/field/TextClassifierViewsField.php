<?php

declare(strict_types=1);

namespace Drupal\openai_text_classifier\Plugin\views\field;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\openai_text_classifier\TextClassifierHelper;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A handler to provide a field for text classifier.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("text_classifier_views_field")
 */
class TextClassifierViewsField extends FieldPluginBase {

  /**
   * Classifier helper.
   *
   * @var \Drupal\openai_text_classifier\TextClassifierHelper
   */
  protected TextClassifierHelper $helper;

  /**
   * TextClassifierViewsField constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin Id.
   * @param object $plugin_definition
   *   Plugin definition.
   * @param \Drupal\openai_text_classifier\TextClassifierHelper $helper
   *   Classifier helper.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TextClassifierHelper $helper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('openai_text_classifier.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);

    if ($entity instanceof NodeInterface &&
      $entity->hasField('body') &&
      !$entity->get('body')->isEmpty()) {

      $label = $this->helper->getNodeLabel($entity);

      $url = Url::fromRoute(
        'openai_text_classifier.api_ajax',
        ['node' => $entity->id()],
        [
          'attributes' => [
            'class' => [
              'use-ajax',
              'label-reload',
            ],
          ],
        ]
      );
      $link = Link::fromTextAndUrl($this->t('Retest'), $url);

      $build = [
        '#theme' => 'classifier_label',
        '#id' => "label-{$entity->bundle()}-{$entity->id()}",
        '#label' => $label,
        '#link' => $link,
        '#cache' => [
          'tags' => ["classifier_label:{$entity->id()}"],
        ],
      ];

    }
    else {
      $build = [
        '#markup' => '',
      ];
    }

    return $build;
  }

}
