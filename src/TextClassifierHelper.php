<?php

declare(strict_types=1);

namespace Drupal\openai_text_classifier;

use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\node\NodeInterface;
use OpenAI\Client;

/**
 * Text Classifier Helper Service.
 */
class TextClassifierHelper {

  /**
   * Logger channel name.
   */
  const LOGGER_CHANNEL = "openai_text_classifier";

  /**
   * Module database table.
   */
  const DB_TABLE = "openai_text_classifier";

  /**
   * Score labels.
   */
  const SCORE_LABELS = [
    [
      "max_score" => 10,
      "assessment" => "very unlikely",
    ],
    [
      "max_score" => 45,
      "assessment" => "unlikely",
    ],
    [
      "max_score" => 90,
      "assessment" => "unclear if it is",
    ],
    [
      "max_score" => 98,
      "assessment" => "possibly",
    ],
    [
      "max_score" => 99,
      "assessment" => "likely",
    ],
  ];

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * OpenAI Client.
   *
   * @var \OpenAI\Client
   */
  protected Client $openai;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * TextClassifierHelper constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection.
   * @param \OpenAI\Client $client
   *   OpenAI api client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger channel factory.
   */
  public function __construct(Connection $connection, Client $client, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->connection = $connection;
    $this->openai = $client;
    $this->logger = $loggerChannelFactory->get(self::LOGGER_CHANNEL);
  }

  /**
   * Returns a label for provided score.
   *
   * @param string $text
   *   Article content.
   *
   * @return string
   *   Score label from API.
   */
  public function getScoreLabel(string $text = ''): string {
    $score = -10;

    try {
      // Request API.
      $response = $this->openai->completions()->create(
        $this->getApiParam($text)
      );

      // Get score from API.
      $result = $response->toArray();
      $score = $result['choices'][0]['logprobs']['top_logprobs'][0]['!'] ?? -10;
      $score = exp($score);

    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }

    // Calculate label using score value.
    $last_index = count(self::SCORE_LABELS) - 1;
    $label = (string) self::SCORE_LABELS[$last_index]['assessment'];

    $score = 100 * (1 - ($score ?? 0));
    for ($i = 0; $i <= $last_index; $i++) {
      if ($score <= self::SCORE_LABELS[$i]['max_score']) {
        $label = (string) self::SCORE_LABELS[$i]['assessment'];
        break;
      }
    }
    return $label;
  }

  /**
   * Save classifier label to database.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node object.
   * @param string $label
   *   Classifier label.
   */
  public function saveNodeLabel(NodeInterface $node, string $label) {
    try {
      // Save classifier label to database.
      $this->connection->merge(self::DB_TABLE)
        ->keys([
          'nid' => $node->id(),
          'bundle' => $node->bundle(),
        ])
        ->fields([
          'classifier' => $label,
        ])
        ->execute();
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
  }

  /**
   * Get classifier label from database.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node object.
   *
   * @return mixed
   *   Node label.
   */
  public function getNodeLabel(NodeInterface $node): mixed {
    $label = "";

    try {
      // Get classifier label from database.
      $label = $this->connection->select(self::DB_TABLE, 'otc')
        ->condition('otc.nid', $node->id())
        ->condition('otc.bundle', $node->bundle())
        ->fields('otc', ['classifier'])
        ->execute()->fetchField(0);

    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }

    return $label ?? '';
  }

  /**
   * Provides static parameters for api request.
   *
   * @param string $prompt
   *   Editor text content.
   *
   * @return array
   *   Static api parameters
   */
  protected function getApiParam($prompt = "") {
    return [
      'model' => 'model-detect-v2',
      'prompt' => $prompt,
      'logprobs' => 5,
      'max_tokens' => 1,
      'n' => 1,
      'stop' => '\n',
      'stream' => FALSE,
      'temperature' => 1,
      'top_p' => 1,
    ];
  }

}
