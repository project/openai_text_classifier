import {Command} from 'ckeditor5/src/core';


export default class TextClassificationCommand extends Command {

  constructor(editor, config) {
    super(editor);
  }

  execute() {
    const editor = this.editor;
    const status = this._status;

    const _html = editor.getData();

    const data = {
      'prompt': _html
    }

    editor.model.change(async writer => {
      const response = await fetch(drupalSettings.path.baseUrl + 'api/openai/text-classifier', {
        method: 'POST',
        credentials: 'same-origin',
        body: JSON.stringify(data),
      });

      if (!response.ok) {
        alert("We encountered an error. Please try again after some time.")
      }

      const result = await response.json();
      if (result?.label) {
        alert(`The classifier considers the text to be very "${result?.label}" AI-generated.`)
      }

    });
  }

}
