import ClassifierUI from './classifierui';
import {Plugin} from 'ckeditor5/src/core';

export default class Classifier extends Plugin {
  // Note that ClassifierUI also extend `Plugin`, but these
  // are not seen as individual plugins by CKEditor 5. CKEditor 5 will only
  // discover the plugins explicitly exported in index.js.
  static get requires() {
    return [ClassifierUI];
  }
}
