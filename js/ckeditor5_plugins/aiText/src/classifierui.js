/**
 * @file registers the classifier toolbar button and binds functionality to it.
 */

import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import TextClassificationCommand
  from "./text_classification/textclassificationcommand";
import icon from '../../../../icons/bot.svg';

export default class ClassifierUI extends Plugin {
  init() {
    const editor = this.editor;

    // Commands.
    editor.commands.add('TextClassificationCommand', new TextClassificationCommand(editor));

    // This will register the classifier toolbar button.
    editor.ui.componentFactory.add('classifier', (locale) => {
      const command = editor.commands.get('TextClassificationCommand');
      const buttonView = new ButtonView(locale);

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('AI Text Classifier'),
        icon,
        tooltip: true,
      });

      // Bind the state of the button to the command.
      buttonView.bind('isOn', 'isEnabled').to(command, 'value', 'isEnabled');

      // Execute the command when the button is clicked (executed).
      this.listenTo(buttonView, 'execute', () =>
        editor.execute('TextClassificationCommand'),
      );

      return buttonView;
    });
  }
}
